- Fare un mini sito one page (unica pagina html), con menu orizzontale 'sopra' (sinistra titolo one page, sulla destra una lista di link che linkano 3 sezioni della della stessa pagina). 

- Il menù deve essere fisso (la barra sopra).

- In pagina principale mettere h1, con immagine (a sinistra ) e paragrafo a destra.

- La prima sezione da raggiungere con il primo link deve contenere un title h2 e due paragrafi ognuno con il proprio title. 

- Questi due paragrafi con titolo e testo vanno messi uno accanto all'altro.

- La seconda sezione a cui punta il menu è una tabella. 

- Sopra tabella titolo h2 'Comparazione'. Tabella con trick: bordi collassati, stile diverso per header e contenuti tabella, ecc...

- La terza sezione con titolo h2 facciamo un mini form dove utilizzare js: un input dove inserire testo e accanto viene riportato testo input.

- implementare un counter (sempre nella terza sezione)

- input date (sempre nella terza sezione)

- Le sezioni devono essere alte almeno quanto lo schermo

- Pulsante in basso a destra per riportarmi su.

- Tutto questo deve essere responsive su desktop

- Su mobile devono andare essere lineari (andare in colonna)

- Per il menù su mobile button dropdown

- (vedere media query)

- Il tutto da mettere in un repository gitLab