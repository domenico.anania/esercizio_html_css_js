let inputNome = document.querySelector('#nome');
let inputCognome = document.querySelector('#cognome');
let inputEmail = document.querySelector('#email');
let inputCodiceF = document.querySelector('#codice');
let pnome = document.querySelector('.nomeInput');
let pcognome = document.querySelector('.cognomeInput');
let pemail = document.querySelector('.emailInput');
let pcodice = document.querySelector('.codiceInput');
let buttonPlus = document.querySelector('.buttonPlus');
let buttonLess = document.querySelector('.buttonLess');
let pcounter = document.querySelector('#counter');
let ul = document.querySelector('ul');
let li = document.querySelectorAll('li');
let btnMenu = document.querySelector('.menu');

let counter = 0;
let display = 'none';

buttonPlus.addEventListener('click', () => {
    counter++;
    pcounter.innerHTML = counter;
})

for (let i = 0; i < li.length; i++) {
    li[i].addEventListener('click', () => {
        ul.style.display = 'none';
    })

}

buttonLess.addEventListener('click', () => {
    if (counter != 0) {
        counter--;
        pcounter.innerHTML = counter;
    } else {
        pcounter.innerHTML = `Mi prendi in giro?? Prova a incrementare`;
    }

})

btnMenu.addEventListener('click', () => {
    if (display == 'none') {
        ul.style.display = 'block';
        display = 'block';
    } else {
        ul.style.display = 'none';
        display = 'none';
    }
})

inputNome.addEventListener('keyup', () => {
    pnome.innerText = inputNome.value;
});

inputCognome.addEventListener('keyup', () => {
    pcognome.innerHTML = inputCognome.value;
});
inputEmail.addEventListener('keyup', () => {
    pemail.innerHTML = inputEmail.value;
});
inputCodiceF.addEventListener('keyup', () => {
    pcodice.innerHTML = inputCodiceF.value;
});

